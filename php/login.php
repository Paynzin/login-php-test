<?php
$database = parse_ini_file("database.ini", true);
$user = $_POST["user"];
$passwd = $_POST["passwd"];
$login_op = $_POST["login_op"];

if ($login_op == "login") {
  $user_status = verifyUser($database, $user, $passwd);

  if ($user_status == 0) {
    echo("User and password are correct!");
  }
  elseif ($user_status == 1) {
    echo("Your password is not correct");
  }
  else {
    echo("The user {$user} does not exist");
  } 
}
else {
  $user_status = registerUser($database, "database.ini", $user, $passwd);

  if ($user_status == 0) {
    echo("User successfully registered");
  }
  else {
    echo("User already exist");
  }
}

  
function verifyUser($database, $user, $passwd)
{
  $tempuser = "user_{$user}";
  if ($database[$tempuser]['username'] != null) {
    if ($database[$tempuser]['passwd'] == $passwd) {
      return 0;
    }
    else {
      return 1;
    }
  }
  else {
    return -1;
  }
}


function registerUser($database, $database_file, $user, $passwd)
{
  $tempuser = "user_{$user}";
  if (verifyUser($database, $user, $passwd) == -1) {
    $database[$tempuser]['username'] = $user;
    $database[$tempuser]['passwd'] = $passwd;
    
    write_ini_file($database_file, $database);

    return 0;
  }
  else {
    return -1;
  }
}


function write_ini_file($file, $array = [])
{
  $data = array();
  foreach ($array as $key => $val) {
    if (is_array($val)) {
      $data[] = "[$key]";
      foreach ($val as $skey => $sval) {
        if (is_array($sval)) {
          foreach ($sval as $_skey => $_sval) {
            if (is_numeric($_skey)) {
              $data[] = $skey.'[] = '.(is_numeric($_sval) ? $_sval : (ctype_upper($_sval) ? $_sval : '"'.$_sval.'"'));
            }
            else {
              $data[] = $skey.'['.$_skey.'] = '.(is_numeric($_sval) ? $_sval : (ctype_upper($_sval) ? $_sval : '"'.$_sval.'"'));
            }
          }
        }
        else {
          $data[] = $skey.' = '.(is_numeric($sval) ? $sval : (ctype_upper($sval) ? $sval : '"'.$sval.'"'));
        }
      }
    }
    else {
      $data[] = $key.' = '.(is_numeric($val) ? $val : (ctype_upper($val) ? $val : '"'.$val.'"'));
    }
    $data[] = null;
  }

  $fp = fopen($file, 'w');
  $retries = 0;
  $max_retries = 100;

  if (!$fp) {
    return false;
  }
  do {
    if ($retries > 0) {
      usleep(rand(1, 5000));
    }
    $retries += 1;
  } while (!flock($fp, LOCK_EX) && $retries <= $max_retries);

  if ($retries == $max_retries) {
    return false;
  }
  
  fwrite($fp, implode(PHP_EOL, $data).PHP_EOL);
  flock($fp, LOCK_UN);
  fclose($fp);

  return true;
}
?>
